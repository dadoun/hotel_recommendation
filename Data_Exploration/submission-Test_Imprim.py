import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
import math
#from pyfm import pylibfm
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse
from scipy.sparse import csr_matrix, lil_matrix
from scipy.sparse import hstack, vstack
from scipy.spatial import distance
from sklearn.metrics.pairwise import cosine_similarity
from Utils import *
from scipy.sparse import linalg
import time


df_train_data = pd.read_csv('../data/dataset_v2/train.csv')
df_test = pd.read_csv('../data/dataset_v2/test.csv')

print("Data loaded OK")

def i_knn(df_train_data):
    # 1st: List of all items
    all_sessions = list(df_train_data.session_id.value_counts().index)
    all_actions = list(df_train_data.action_type.value_counts().index)
    
    dict_items_sessions_sim = dict()
    matrix_train_data_views = df_train_data[df_train_data['action_type'].isin(['clickout item', 'interaction item rating', 'interaction item image', 'interaction item rating', 'interaction item info', 'interaction item deals', 'search for item'])].drop_duplicates(['session_id', 'reference']).values
    
    dict_all_sessions = dict(zip(all_sessions, np.arange(0,len(all_sessions),1)))
    
    current_session_id = 0;
    for i in range(len(matrix_train_data_views)):
        new_session_id = matrix_train_data_views[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            current_reference = matrix_train_data_views[i,5]
            if not(current_reference in dict_items_sessions_sim):
                dict_items_sessions_sim[current_reference] = lil_matrix((1, len(all_sessions)), dtype=np.int8)
            vec = dict_items_sessions_sim[current_reference]
            vec[0,index_session] = 1
            dict_items_sessions_sim[current_reference] = vec               
        else:
            current_session_id = new_session_id
            index_session = dict_all_sessions[current_session_id]
            current_reference = matrix_train_data_views[i,5]
            if not(current_reference in dict_items_sessions_sim):
                dict_items_sessions_sim[current_reference] = lil_matrix((1, len(all_sessions)), dtype=np.int8)
            vec = dict_items_sessions_sim[current_reference]
            vec[0,index_session] = 1
            dict_items_sessions_sim[current_reference] = vec
        
    return dict_items_sessions_sim

df_total = pd.concat([df_test, df_train_data])
df_total.reference.fillna('NAN', inplace=True)
df_tot = df_total[df_total['reference'] != 'NAN']

print("df_Tot OK")

dict_items_sessions_sim = i_knn(df_tot)

print("Dictionary OK")

def fastest_cosine_sim_(item1, item2, dict_items_sessions_sim):
    nzz_vec_1 = dict_items_sessions_sim[item1].nonzero()[1]
    nzz_vec_2 = dict_items_sessions_sim[item2].nonzero()[1]
    len_1 = len(nzz_vec_1)
    len_2 = len(nzz_vec_2)
    cnt = 0
    if len_1 < len_2:
        for k in nzz_vec_1:
            if k in nzz_vec_2:
                cnt += 1
    else:
        for k in nzz_vec_2:
            if k in nzz_vec_1:
                cnt += 1
    norm_1 = math.sqrt(len_1)
    norm_2 = math.sqrt(len_2)
    return cnt/(norm_1*norm_2)


df_pop = pd.read_csv('../data/dataset_v2/submission_popular.csv')
matrix_submissions = df_pop[['user_id', 'session_id', 'step', 'item_recommendations']].values
User_Session_Step = list()
for el in matrix_submissions:
    User_Session_Step.append(str(el[0])+str(el[1])+str(el[2]))
    
matrix_test = df_test[['user_id', 'session_id', 'step', 'impressions', 'prices',
                       'action_type', 'reference', 'platform', 'city', 'device', 'current_filters']].values
###########################
current_session_id = 0; current_reference = 0; current_action = 0; list_new_matrix = list()
for i in range(len(matrix_test)):
    new_session_id = matrix_test[i,1]
    new_user_id = matrix_test[i,0]
    new_step_id = matrix_test[i,2]
    ## if the same session
    if new_session_id == current_session_id:
        new_reference = matrix_test[i,6]
        new_action = matrix_test[i,5]
        if new_action == 'clickout item':
            list_new_matrix.append(matrix_test[i,:])
        elif new_action == current_action and new_reference == current_reference:
            k =0
        else:
            list_new_matrix.append(matrix_test[i,:])
    else:
        current_session_id = new_session_id
        current_reference = matrix_test[i,6]
        current_action = matrix_test[i,5]
        list_new_matrix.append(matrix_test[i,:])
###########################
new_matrix_test = np.array(list_new_matrix)
###########################
dict_test = dict()
for el in new_matrix_test:
    dict_test[str(el[0])+str(el[1])+str(el[2])] = str(el[3]) + ';' + str(el[4]) + ';' + str(el[5]) + ';' + str(el[6]) + ';' + str(el[7]) + ';' + str(el[8]) + ';' + str(el[9]) + ';' + str(el[10])
    
print("Data OK for prediction")

start = time.time()

list_impressions = list()
## Loop Over all the submissions
for i in range(len(matrix_submissions)):
    if not(i%5000):
        print(i)
    # Step Number
    current_step = matrix_submissions[i,2]
    session_id = matrix_submissions[i,1]
    user_id = matrix_submissions[i,0]
    EL = dict_test[User_Session_Step[i]].split(';')
    impressions_test = EL[0].split('|')
    #print("Old impressions")
    #print(impressions_test)
    # Retrieve the action in the main test df
    prev_actions_ref = list()
    for h in range(current_step):
        curr_step = h + 1
        key = str(user_id) + str(session_id) + str(curr_step)
        if key in dict_test:
            EL_Moins_i = dict_test[key].split(';')
            prev_actions_ref.append(EL_Moins_i[3])
    if len(prev_actions_ref) > 5:
        prev_actions_ref = prev_actions_ref[0:6]
    total_range_items = list()
    for pref_ref in prev_actions_ref:
        if pref_ref in dict_items_sessions_sim:
            range_items = list()
            for item in impressions_test:
                if item in dict_items_sessions_sim:
                    range_items.append(fastest_cosine_sim_(item, pref_ref, dict_items_sessions_sim))
                else:
                    range_items.append(0)
            total_range_items.append(np.array(range_items))
    counter = np.zeros(len(range_items))
    for item_cnt in total_range_items:
        for k in range(len(counter)):
            counter[k] += item_cnt[k]
    if len(total_range_items) > 0:
        counter_new = counter/len(total_range_items)
        df_2_sort = pd.DataFrame(data=list(counter_new))
        descending_arg_sort = list(df_2_sort.sort_values(by=0, ascending=False, kind= 'mergesort').index)
        #print("similarities")
        #print(counter_new)
        impressions_test = [impressions_test[j] for j in descending_arg_sort]
        #print("New impressions")
        #print(impressions_test)
        #print("**************************")
        #if i > 10:
        #    break
    list_impressions.append(" ".join(impressions_test))
list_knn_sub = list_impressions
print("hello")
end = time.time()
print(end - start)

df_pop['item_recommendations'] = list_knn_sub

df_pop.to_csv('../data/New_submission_KNN.csv')