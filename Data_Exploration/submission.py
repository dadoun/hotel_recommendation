import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
import math
#from pyfm import pylibfm
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse
from scipy.sparse import csr_matrix, lil_matrix
from scipy.sparse import hstack, vstack
from scipy.spatial import distance
from sklearn.metrics.pairwise import cosine_similarity
from Utils import *
from scipy.sparse import linalg
import time


df_train_data = pd.read_csv('../data/dataset_v2/train.csv')
df_test = pd.read_csv('../data/dataset_v2/test.csv')

print("Data loaded OK")

df_pop = pd.read_csv('../data/dataset_v2/submission_popular.csv')
matrix_submissions = df_pop[['user_id', 'session_id', 'step', 'item_recommendations']].values
User_Session_Step = list()
for el in matrix_submissions:
    User_Session_Step.append(str(el[0])+str(el[1])+str(el[2]))
    
matrix_test = df_test[['user_id', 'session_id', 'step', 'impressions', 'prices',
                       'action_type', 'reference', 'platform', 'city', 'device', 'current_filters']].values
###########################
current_session_id = 0; current_reference = 0; current_action = 0; list_new_matrix = list()
for i in range(len(matrix_test)):
    new_session_id = matrix_test[i,1]
    new_user_id = matrix_test[i,0]
    new_step_id = matrix_test[i,2]
    ## if the same session
    if new_session_id == current_session_id:
        new_reference = matrix_test[i,6]
        new_action = matrix_test[i,5]
        if new_action == 'clickout item':
            list_new_matrix.append(matrix_test[i,:])
        elif new_action == current_action and new_reference == current_reference:
            k =0
        else:
            list_new_matrix.append(matrix_test[i,:])
    else:
        current_session_id = new_session_id
        current_reference = matrix_test[i,6]
        current_action = matrix_test[i,5]
        list_new_matrix.append(matrix_test[i,:])
###########################
new_matrix_test = np.array(list_new_matrix)
###########################
dict_test = dict()
for el in new_matrix_test:
    dict_test[str(el[0])+str(el[1])+str(el[2])] = str(el[3]) + ';' + str(el[4]) + ';' + str(el[5]) + ';' + str(el[6]) + ';' + str(el[7]) + ';' + str(el[8]) + ';' + str(el[9]) + ';' + str(el[10])
    
print("Data OK for prediction")

list_impressions = list()
case_1_click = 0;case_2_click = 0;case_3_click = 0;
case_4_click = 0;case_5_click = 0;case_6_click = 0;
case_7_click = 0;case_8_click = 0;case_9_click = 0;
case_1_prim_click = 0; case_2_prim_click = 0; case_3_prim_click = 0; case_4_prim_click = 0; case_user = 0; dict_cases=dict();
indict_test = 0; case_in_click = 0
## Loop Over all the submissions
for i in range(len(matrix_submissions)):
    # Step Number
    current_step = matrix_submissions[i,2]
    # Retrieve the action in the main test df
    EL = dict_test[User_Session_Step[i]].split(';')
    impressions_test = EL[0].split('|')
    prices_test = EL[1].split('|')
    if current_step < 2:
        case_1_click += 1
        list_impressions.append(" ".join(impressions_test))
    # if not we see the previous actions
    else:
        case_2_click += 1
        step_moins_1 = current_step - 1
        user_session = User_Session_Step[i][0:len(User_Session_Step[i])-len(str(current_step))]
        EL_Moins_1 = dict_test[str(user_session) + str(step_moins_1)].split(';')
        if EL_Moins_1[3] in impressions_test:
            case_3_click += 1 
            impressions_test.remove(EL_Moins_1[3])
            impressions_test.insert(0, EL_Moins_1[3])
            list_impressions.append(" ".join(impressions_test))
        elif step_moins_1 <2:
            case_4_click += 1
            list_impressions.append(" ".join(impressions_test))
        else:
            step_moins_2 = step_moins_1 - 1
            user_session = User_Session_Step[i][0:len(User_Session_Step[i])-len(str(current_step))]
            EL_Moins_2 = dict_test[str(user_session) + str(step_moins_2)].split(';')
            if EL_Moins_2[3] in impressions_test:
                case_5_click += 1 
                impressions_test.remove(EL_Moins_2[3])
                impressions_test.insert(0, EL_Moins_2[3])
                list_impressions.append(" ".join(impressions_test))
            else:
                list_impressions.append(" ".join(impressions_test))

df_pop['item_recommendations'] = list_impressions

df_pop.to_csv('../data/Submission_Test.csv')