import tensorflow as tf
import pandas as pd
import numpy as np
import os
import sys
sys.path.insert(0, '/mnt/sdb/adadoun/PhD/hotel_recommendation')
from Utils import *
#from gensim.models import Word2Vec
import random
import pickle
import time



## Load the data
dict_action_ref_id = load_obj(name='dict_action_ref_id')

with open("../train_data_bpr/train_16.pkl", "rb") as fp:   # Unpickling
    train_bpr = pickle.load(fp)
with open("../train_data_bpr/test_16.pkl", "rb") as fp:   # Unpickling
    test_bpr = pickle.load(fp)
with open("../train_data_bpr/val_16.pkl", "rb") as fp:   # Unpickling
    val_bpr = pickle.load(fp)
with open("../train_data_bpr/test_ids_16.pkl", "rb") as fp:   # Unpickling
    test_ids_bpr = pickle.load(fp)
with open("../train_data_bpr/val_ids_16.pkl", "rb") as fp:   # Unpickling
    val_ids_bpr = pickle.load(fp)

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"
config = tf.ConfigProto()
config.gpu_options.allow_growth=True

# Parameters
epsilon = 1e-10
# Max length of sentences
max_l = 16

# Parameters
learning_rate = 0.003
training_epochs = 5
batch_size = 256
hidden_states = 128
emb_size = 256

train_new_bpr = train_bpr[0:100000]
val_new_bpr = val_bpr[0:10000]
data_bpr = Dataset_BPR(train_bpr, val_new_bpr, test_bpr, dict_action_ref_id, max_l=16)
nb_item = len(dict_action_ref_id)

hidden_states = 128
tf.reset_default_graph()
tf.set_random_seed(123)
model_path = "models_16_bpr/gru_.ckpt"
# tf Graph Input:  sentiment analysis data
x_data = tf.placeholder(tf.int32, [None, max_l, 1], name='InputData')
# masks
m = tf.placeholder(tf.float32, [None, max_l, 1], name='MaskData')
# Positive (1) or Negative (0) labels
pos = tf.placeholder(tf.int32, [None], name='Pos_Data')
# Neg
neg = tf.placeholder(tf.int32, [None], name='Neg_Data')

# Embedding Layer
inputs_emb_w = tf.get_variable("input_emb", [nb_item, 256], 
            initializer=tf.random_normal_initializer(0, 0.1))

item_b = tf.get_variable("item_b", [nb_item, 1], 
                        initializer=tf.constant_initializer(0.0))
#

seq_emb = tf.nn.embedding_lookup(inputs_emb_w, x_data)

#
pos_emb = tf.nn.embedding_lookup(inputs_emb_w, pos)
neg_emb = tf.nn.embedding_lookup(inputs_emb_w, neg)
pos_b = tf.nn.embedding_lookup(item_b, pos)
neg_b = tf.nn.embedding_lookup(item_b, neg)
#

#
gru = GRU(256, hidden_states)

#
gru_output = process_sequence(gru, seq_emb, m, max_l)


W_pos = tf.Variable(tf.random_normal([256, 128]), name='Weights')
b_pos = tf.Variable(tf.random_normal([128]), name='Bias')

#W_neg = tf.Variable(tf.random_normal([256, 128]), name='Weights')
#b_neg = tf.Variable(tf.random_normal([128]), name='Bias')

pred_pos_output = tf.nn.sigmoid(tf.matmul(pos_emb, W_pos) + b_pos)
pred_neg_output = tf.nn.sigmoid(tf.matmul(neg_emb, W_pos) + b_pos)

with tf.name_scope('Loss'):
    # Minimize error using BPR Loss
    
    # MF predict: u_i > u_j
    x = pos_b - neg_b + tf.reduce_sum(tf.multiply(gru_output, pred_pos_output-pred_neg_output), 1, keep_dims=True)
    
    l2_norm = tf.add_n([ 
            tf.reduce_sum(tf.multiply(pos_emb, pos_emb)),
            tf.reduce_sum(tf.multiply(neg_emb, neg_emb))
        ])
    
    lamnbda = 0.0001
    cost = lamnbda * l2_norm - tf.reduce_mean(tf.log(tf.sigmoid(x)))
    
with tf.name_scope('Adam'):
    # Gradient Descent
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

# Initializing the variables
init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session(config=config) as sess:
    sess.run(init)
    # Training cycle
    print("Training started")
    best_val_loss = 100.
    for epoch in range(training_epochs):
        model_path = "models_16_bpr/gru_" + str(epoch) + ".ckpt"
        avg_cost = 0.
        total_batch = int(len(train_bpr)/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_actions, batch_pos, batch_neg, batch_m  = data_bpr.next_batch(batch_size, start=i*batch_size, end=(i+1)*batch_size)
            # Run optimization op (backprop), cost op (to get loss value)
            # and summary nodes
            _, c = sess.run([optimizer, cost],
                                     feed_dict={x_data: batch_actions, m: batch_m, pos:batch_pos, neg:batch_neg})
            # Compute average loss
            avg_cost += c / total_batch
        # Display logs per epoch step
        val_actions, val_pos, val_neg, val_m = data_bpr.val_batch()
        x_eval = x.eval({x_data: val_actions, m: val_m, pos:val_pos, neg:val_neg})
        print("X Diff:", x_eval)
        val_loss = cost.eval({x_data: val_actions, m: val_m, pos:val_pos, neg:val_neg})
        print("Loss on validation:", val_loss)
        #if val_loss < best_val_loss:
        #    best_val_loss = val_loss
        save_path = saver.save(sess, model_path)
        print("        Model saved in file: %s" % save_path)
        print("Epoch: ", '%02d' % (epoch+1), "  =====> Loss=", "{:.9f}".format(avg_cost))