import tensorflow as tf
import pandas as pd
import numpy as np
import os
import sys
sys.path.insert(0, '/mnt/sdb/adadoun/PhD/hotel_recommendation')
from Utils import *
#from gensim.models import Word2Vec
import random
import pickle
import time



## Load the data

dict_action_ref_id = load_obj(name='dict_action_ref_id')
dict_hotel_property = load_obj(name='dict_hotel_property')

with open("../train_data/train_16.pkl", "rb") as fp:   # Unpickling
    train = pickle.load(fp)
with open("../train_data/test_16.pkl", "rb") as fp:   # Unpickling
    test = pickle.load(fp)
with open("../train_data/val_16.pkl", "rb") as fp:   # Unpickling
    val = pickle.load(fp)
with open("../train_data/test_ids_16.pkl", "rb") as fp:   # Unpickling
    test_ids = pickle.load(fp)
with open("../train_data/val_ids_16.pkl", "rb") as fp:   # Unpickling
    val_ids = pickle.load(fp)

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"
config = tf.ConfigProto()
config.gpu_options.allow_growth=True

# Parameters
epsilon = 1e-10
max_l = 16# Max length of sentences

# Parameters
batch_size = 128
learning_rate = 0.001
training_epochs = 1
hidden_states = 128

val_new = val[0:1000]
data = Dataset_Content(train, val_new, test, dict_action_ref_id, max_l, dict_hotel_property)
nb_item = len(dict_action_ref_id)

tf.reset_default_graph()
tf.set_random_seed(123)
model_path = "models_16_content/gru_.ckpt"
# tf Graph Input:  sentiment analysis data
x_data = tf.placeholder(tf.int32, [None, max_l, 1], name='InputData')
# masks
m = tf.placeholder(tf.float32, [None, max_l, 1], name='MaskData')
# Positive (1) or Negative (0) labels
y = tf.placeholder(tf.float32, [None, 1], name='LabelData')
# Hotel properties
hotel_prty = tf.placeholder(tf.float32, [None, 157], name='hotel_prty')

#
inputs_emb_w = tf.get_variable("input_emb", [nb_item, 256], 
            initializer=tf.random_normal_initializer(0, 0.1))
u_emb = tf.nn.embedding_lookup(inputs_emb_w, x_data)

#
gru = GRU(256, hidden_states)

#
gru_output = process_sequence(gru, u_emb, m, max_l)

W_content = tf.Variable(tf.random_normal([157, 128]), name='Weights_Content')
b_content = tf.Variable(tf.random_normal([128]), name='Bias_Content')
content_vector_output = tf.nn.sigmoid(tf.matmul(hotel_prty, W_content) + b_content)

mult_vector = tf.multiply(gru_output, content_vector_output)

W = tf.Variable(tf.random_normal([hidden_states, 1]), name='Weights')
b = tf.Variable(tf.random_normal([1]), name='Bias')
pred = tf.nn.sigmoid(tf.matmul(mult_vector, W) + b)

with tf.name_scope('Loss'):
    # Minimize error using cross entropy
    # We use tf.clip_by_value to avoid having too low numbers in the log function
    cost = tf.reduce_mean(-y*tf.log(tf.clip_by_value(pred, epsilon, 1.0)) - (1.-y)*tf.log(tf.clip_by_value((1.-pred), epsilon, 1.0)))

with tf.name_scope('Adam'):
    # Gradient Descent
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
with tf.name_scope('Accuracy'):
    # Accuracy
    pred_tmp = tf.stack([pred, 1.-pred])
    y_tmp = tf.stack([y, 1.-y])
    acc = tf.equal(tf.argmax(pred_tmp, 0), tf.argmax(y_tmp, 0))
    acc = tf.reduce_mean(tf.cast(acc, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()
saver = tf.train.Saver()

with tf.Session(config=config) as sess:
    sess.run(init)
    # Training cycle
    print("Training started")
    best_val_loss = 100.
    for epoch in range(training_epochs):
        model_path = "models_16_content/gru_" + str(epoch) + ".ckpt"
        avg_cost = 0.
        total_batch = int(len(train)/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_actions, batch_y, batch_hotel_content, batch_m  = data.next_batch(batch_size, start=i*batch_size, end=(i+1)*batch_size)
            # Run optimization op (backprop), cost op (to get loss value)
            # and summary nodes
            _, c = sess.run([optimizer, cost],
                                     feed_dict={x_data: batch_actions, m: batch_m, y:batch_y, hotel_prty: batch_hotel_content})
            # Compute average loss
            avg_cost += c / total_batch
            K_4 = int(total_batch/5)
            list_K_4 = [K_4, 2*K_4, 3*K_4, 4*K_4]
            if i in list_K_4:
                model_path = "models_16_content/gru_" + str(epoch) + "_" +  str(i) + ".ckpt"
                save_path = saver.save(sess, model_path)
                print("        Model saved in file: %s" % save_path)
                
        print("Training Finished")
        # Display logs per epoch step
        val_actions, val_y, val_hotel_prty, val_ms = data.val_batch()
        val_loss = cost.eval({x_data: val_actions, m: val_ms, y: val_y, hotel_prty: val_hotel_prty})
        print("Loss on validation:", val_loss)
        save_path = saver.save(sess, model_path)
        print("        Model saved in file: %s" % save_path)
        print("Epoch: ", '%02d' % (epoch+1), "  =====> Loss=", "{:.9f}".format(avg_cost))