import tensorflow as tf
import pandas as pd
import numpy as np
import os
import sys
sys.path.insert(0, '/mnt/sdb/adadoun/PhD/hotel_recommendation')
from Utils import *
#from gensim.models import Word2Vec
import random
import pickle
import time



## Load the data

'''
df_train_data = pd.read_csv('../data/dataset_v2/train.csv')
df_test = pd.read_csv('../data/dataset_v2/test.csv')

## Encoder dictionary
dict_action_ref_id = action_ref_encoding(df_train_data, df_test)

corpus_labeled_train = get_seq_labeled_(df_train_data, Ns=3, phase='train')
df_test.reference.fillna('NAN', inplace=True)
corpus_labeled_eval_test = get_seq_labeled_(df_test, Ns=3 ,phase='test')

train, val, test, val_ids, test_ids = get_train_val_test(corpus_labeled_train, corpus_labeled_eval_test, max_l = 16)
'''

dict_action_ref_id = load_obj(name='dict_action_ref_id')


with open("../train_data/train_16.pkl", "rb") as fp:   # Unpickling
    train = pickle.load(fp)
with open("../train_data/test_16.pkl", "rb") as fp:   # Unpickling
    test = pickle.load(fp)
with open("../train_data/val_16.pkl", "rb") as fp:   # Unpickling
    val = pickle.load(fp)
with open("../train_data/test_ids_16.pkl", "rb") as fp:   # Unpickling
    test_ids = pickle.load(fp)
with open("../train_data/val_ids_16.pkl", "rb") as fp:   # Unpickling
    val_ids = pickle.load(fp)

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"
config = tf.ConfigProto()
config.gpu_options.allow_growth=True

# Parameters
epsilon = 1e-10
max_l = 16# Max length of sentences

# Parameters
learning_rate = 0.003
training_epochs = 2
batch_size = 256
hidden_states = 128
emb_size = 256

val_new = val[0:1000]
train_new = train[0:100000]
data = Dataset(train, val_new, test, dict_action_ref_id, max_l)
nb_item = len(dict_action_ref_id)

tf.reset_default_graph()
tf.set_random_seed(123)
model_path = "models_16_4/gru_.ckpt"
# tf Graph Input:  sentiment analysis data
x_data = tf.placeholder(tf.int32, [None, max_l, 1], name='InputData')
# masks
m = tf.placeholder(tf.float32, [None, max_l, 1], name='MaskData')
# Positive (1) or Negative (0) labels
y = tf.placeholder(tf.float32, [None, 1], name='LabelData')

#
inputs_emb_w = tf.get_variable("input_emb", [nb_item, emb_size], 
            initializer=tf.random_normal_initializer(0, 0.1))
u_emb = tf.nn.embedding_lookup(inputs_emb_w, x_data)

#
gru = GRU(emb_size, hidden_states)

#
gru_output = process_sequence(gru, u_emb, m, max_l)

W = tf.Variable(tf.random_normal([hidden_states, 1]), name='Weights')
b = tf.Variable(tf.random_normal([1]), name='Bias')

pred = tf.nn.sigmoid(tf.matmul(gru_output, W) + b)

with tf.name_scope('Loss'):
    # Minimize error using cross entropy
    # We use tf.clip_by_value to avoid having too low numbers in the log function
    cost = tf.reduce_mean(-y*tf.log(tf.clip_by_value(pred, epsilon, 1.0)) - (1.-y)*tf.log(tf.clip_by_value((1.-pred), epsilon, 1.0)))

with tf.name_scope('Adam'):
    # Gradient Descent
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
with tf.name_scope('Accuracy'):
    # Accuracy
    pred_tmp = tf.stack([pred, 1.-pred])
    y_tmp = tf.stack([y, 1.-y])
    acc = tf.equal(tf.argmax(pred_tmp, 0), tf.argmax(y_tmp, 0))
    acc = tf.reduce_mean(tf.cast(acc, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()
saver = tf.train.Saver()
with tf.Session(config=config) as sess:
    sess.run(init)
    # Training cycle
    print("Training started")
    best_val_loss = 100.
    for epoch in range(training_epochs):
         
        avg_cost = 0.
        total_batch = int(len(train)/batch_size)
        K_4 = int(total_batch/5)
        list_K_4 = [K_4, 2*K_4, 3*K_4, 4*K_4]
        # Loop over all batches
        for i in range(total_batch):
            batch_actions, batch_y, batch_ms  = data.next_batch(batch_size, start=i*batch_size, end=(i+1)*batch_size)
            # Run optimization op (backprop), cost op (to get loss value)
            # and summary nodes
            _, c = sess.run([optimizer, cost],
                                     feed_dict={x_data: batch_actions, m: batch_ms, y:batch_y})
            # Compute average loss
            avg_cost += c / total_batch
            if i in list_K_4:
                model_path = "models_16_4/gru_" + str(epoch) + "_" +  str(i) + ".ckpt"
                save_path = saver.save(sess, model_path)
                print("        Model saved in file: %s" % save_path)
        model_path = "models_16_4/gru_" + str(epoch) + ".ckpt"
        # Display logs per epoch step
        val_actions, val_y, val_ms = data.val_batch()
        val_acc = acc.eval({x_data: val_actions, m: val_ms, y: val_y})
        print("Accuracy on validation:", val_acc)
        val_loss = cost.eval({x_data: val_actions, m: val_ms, y: val_y})
        print("Loss on validation:", val_loss)
        save_path = saver.save(sess, model_path)
        print("        Model saved in file: %s" % save_path)
        print("Epoch: ", '%02d' % (epoch+1), "  =====> Loss=", "{:.9f}".format(avg_cost))