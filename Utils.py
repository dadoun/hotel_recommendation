import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
import tensorflow as tf
import math
#from pyfm import pylibfm
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse
from scipy.sparse import csr_matrix, lil_matrix
from scipy.sparse import hstack, vstack
from scipy.spatial import distance
from sklearn.metrics.pairwise import cosine_similarity
from ampligraph.latent_features import TransE
from gensim.models import Word2Vec, FastText
from sklearn.preprocessing import normalize, scale, MinMaxScaler, StandardScaler
import random
import json
import numpy as np
import random
import pickle

random.seed(1234)
 # max length of sentences

    
def get_train_val_test_bpr(corpus_labeled_train, corpus_labeled_eval_test, max_l = 8):
    train = list();
    for item in corpus_labeled_train:
        if len(item)==3:
            if len(item[2]) == 2:
                pos_item = item[2][0]
                neg_item = item[2][1]
                seq_actions = item[1]
                seq_actions.append('EOS')
                if len(seq_actions) < max_l:
                    train.append((seq_actions, pos_item, neg_item))
                else:
                    train.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], pos_item, neg_item))
    val = list(); test = list(); test_ids = list(); val_ids = list()
    for item in corpus_labeled_eval_test:
        if len(item) == 3:
            if len(item[2]) == 2:
                pos_item = item[2][0]
                neg_item = item[2][1]
                seq_actions = item[1]
                seq_actions.append('EOS')
                if len(seq_actions) < max_l:
                    val.append((seq_actions, pos_item, neg_item))
                else:
                    val.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], pos_item, neg_item))
                val_ids.append(item[0])
            elif item[2] == 'test':
                y = item[2]
                seq_actions = item[1]
                seq_actions.append('EOS')
                if len(seq_actions) < max_l:
                    test.append((seq_actions, y))
                else:
                    test.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], y))
                test_ids.append(item[0])
    return train, val, test, val_ids, test_ids

def get_train_val_test(corpus_labeled_train, corpus_labeled_eval_test, max_l = 8):
    train = list();
    for item in corpus_labeled_train:
        if len(item)==3:
            if item[2] == 0 or item[2] == 1:
                y = item[2]
                seq_actions = item[1]
                if len(seq_actions) < max_l:
                    train.append((seq_actions, y))
                else:
                    train.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], y))
    val = list(); test = list(); test_ids = list(); val_ids = list()
    for item in corpus_labeled_eval_test:
        if len(item) == 3:
            if item[2] == 0 or item[2] == 1:
                y = item[2]
                seq_actions = item[1]
                if len(seq_actions) < max_l:
                    val.append((seq_actions, y))
                else:
                    val.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], y))
                val_ids.append(item[0])
            elif item[2] == 'test':
                y = item[2]
                seq_actions = item[1]
                if len(seq_actions) < max_l:
                    test.append((seq_actions, y))
                else:
                    test.append((seq_actions[len(seq_actions)-max_l:len(seq_actions)], y))
                test_ids.append(item[0])
    return train, val, test, val_ids, test_ids

def action_ref_encoding(df_train_data, df_test):
    df_total = pd.concat([df_train_data, df_test])
    df_total.reference.fillna('NAN', inplace=True)
    dict_action_ref_id = dict()
    matrix_total = df_total.values
    cnt_id = 0
    for i in range(len(matrix_total)):
        action_id = matrix_total[i,4]
        reference_id = matrix_total[i,5]
        if reference_id != 'NAN':
            key = reference_id #"|".join([reference_id, action_id])
            if not(key in dict_action_ref_id):
                dict_action_ref_id[key] = cnt_id
                cnt_id += 1
        elif action_id != 'clickout item':
            key = reference_id#"|".join([reference_id, action_id])
            if not(key in dict_action_ref_id):
                dict_action_ref_id[key] = cnt_id
                cnt_id += 1
        else:
            impression_list = matrix_total[i,10].split("|")
            for item in impression_list:
                key = reference_id#"|".join([reference_id, item])
                if not(key in dict_action_ref_id):
                    dict_action_ref_id[key] = cnt_id
                    cnt_id += 1
    return dict_action_ref_id

def hotel_properties(df_items):
    
    dict_items_prt = dict(zip(df_items.values[:,0], df_items.values[:,1]))
    df_properties = df_items.values[:,1]
    list_nb_prt = list()
    for prt in df_properties:
        nb_prt = len(prt.split('|'))
        list_nb_prt.append(nb_prt)
    df_items['Nb_properties'] = list_nb_prt
    df_properties = df_items.values[:,1]
    dict_prt_occ = dict()
    for prt in df_properties:
        prts = prt.split('|')
        for prt_i in prts:
            if prt_i in dict_prt_occ:
                dict_prt_occ[prt_i] += 1
            else:
                dict_prt_occ[prt_i] = 1
    
    list_prts = list(dict_prt_occ.keys())
    property_representation = list()
    for key in dict_items_prt:
        acc_representation = np.zeros((1,len(list_prts)))
        for i, prt in enumerate(list_prts):
            l = dict_items_prt[key].split('|')
            if prt in l:
                acc_representation[0,i] = 1
        property_representation.append(acc_representation)
    np_property_representation = np.array(property_representation)
    dict_hotel_property = dict(zip(list(dict_items_prt.keys()), property_representation))
        
    
    return df_items, dict_prt_occ, list_prts, dict_hotel_property

def ucb_computation(df_train_data, df_test):
    
    ## First: compute the interaction number of each product
    dict_inter = dict()
    matrix_total = df_train_data.values
    for row in matrix_total:
        if row[4] == 'interaction item image' or row[4] == 'interaction item rating' or row[4] == 'interaction item info' or row[4] == 'interaction item deals' or row[4] == 'search for item':
            if row[5] in dict_inter:
                dict_inter[row[5]] += 1
            else:
                dict_inter[row[5]] = 1
            
    ## Second: compute the impression number and nb_click of each product  
    dict_imp = dict()
    dict_click = dict()
    for row in matrix_total:
        if row[4] == 'clickout item' :
            if row[5] in dict_click:
                dict_click[row[5]] += 1
            else:
                dict_click[row[5]] = 1
            impressions_list = row[10]
            elements = impressions_list.split('|')
            for el in elements:
                if el in dict_imp:
                    dict_imp[el] += 1
                else:
                    dict_imp[el] = 1
                    
    ## Third compute the cr
    dict_rate = dict()
    for key in dict_imp:
        if key in dict_click:
            dict_rate[key] = dict_click[key]/dict_imp[key]
        else:
            dict_rate[key] = 0
    
    ### UCB Algo
    ### Reorder the impressions following the UCB score
    df_test.reference.fillna('NAN', inplace=True)
    df_sub = df_test[(df_test['reference'] == 'NAN') &  (df_test['action_type'] == 'clickout item')]
    impression_items = df_sub['impressions'].values
    list_ucb_impressions = list()
    z =0
    for impression in impression_items:
        z += 1
        pop_impression = impression.split(" ")
        T_impressions = 0
        for item in pop_impression:
            if item in dict_inter:
                T_impressions += dict_inter[item]
        range_ucb = list()
        for item in pop_impression:
            if item in dict_rate:
                p = dict_rate[item]
            else :
                p = 0
            if item in dict_inter:
                N = dict_inter[item]
            else :
                N = 0
            if N == 0:
                range_ucb.append(0)
            else:
                term_0 = 2*(math.log(T_impressions)/N)
                term_1 = 0.1*math.sqrt(term_0)
                ucb_score = p + term_1
                range_ucb.append(ucb_score)
        df_2_sort = pd.DataFrame(data=range_ucb)
        descending_arg_sort = list(df_2_sort.sort_values(by=0, ascending=False, kind= 'mergesort').index)
        ucb_impressions = [pop_impression[j] for j in descending_arg_sort]
        list_ucb_impressions.append(" ".join(ucb_impressions))
            
    return list_ucb_impressions
            
def association_rules(df_train_data):
    matrix_train_data = df_train_data.values
    current_session_id = 0; dict_ar = dict();current_reference = 0
    for i in range(len(matrix_train_data)):
        new_session_id = matrix_train_data[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_train_data[i,5]
            association_reference = str(current_reference) + '|' + str(new_reference)
            inv_association_reference = str(new_reference) + '|' + str(current_reference)
            if association_reference in dict_ar:
                dict_ar[association_reference] += 1
            elif inv_association_reference in dict_ar:
                dict_ar[inv_association_reference] += 1
            else:
                 dict_ar[association_reference] = 1
            current_reference = new_reference
        else:
            current_session_id = new_session_id
            current_reference = matrix_train_data[i,5]
    return dict_ar
            
            
def markov_chains(df_train_data):
    matrix_train_data_views = df_train_data.values
    current_session_id = 0; dict_sr = dict();current_reference = 0
    for i in range(len(matrix_train_data_views)):
        new_session_id = matrix_train_data_views[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_train_data_views[i,5]
            if new_reference != current_reference:
                association_reference = str(current_reference) + '|' + str(new_reference)
                if association_reference in dict_sr:
                    dict_sr[association_reference] += 1
                else:
                    dict_sr[association_reference] = 1
            current_reference = new_reference
        else:
            current_session_id = new_session_id
            current_reference = matrix_train_data_views[i,5]            
    return dict_sr
            
def i_knn(df_train_data):
    # 1st: List of all items
    all_sessions = list(df_train_data.session_id.value_counts().index)
    all_actions = list(df_train_data.action_type.value_counts().index)
    
    dict_items_sessions_sim = dict()
    matrix_train_data_views = df_train_data[df_train_data['action_type'].isin(['clickout item', 'interaction item rating', 'interaction item image', 'interaction item rating', 'interaction item info', 'interaction item deals', 'search for item'])].drop_duplicates(['session_id', 'reference']).values
    
    dict_all_sessions = dict(zip(all_sessions, np.arange(0,len(all_sessions),1)))
    
    current_session_id = 0;
    for i in range(len(matrix_train_data_views)):
        new_session_id = matrix_train_data_views[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            current_reference = matrix_train_data_views[i,5]
            if not(current_reference in dict_items_sessions_sim):
                dict_items_sessions_sim[current_reference] = lil_matrix((1, len(all_sessions)), dtype=np.int8)
            vec = dict_items_sessions_sim[current_reference]
            vec[0,index_session] = 1
            dict_items_sessions_sim[current_reference] = vec               
        else:
            current_session_id = new_session_id
            index_session = dict_all_sessions[current_session_id]
            current_reference = matrix_train_data_views[i,5]
            if not(current_reference in dict_items_sessions_sim):
                dict_items_sessions_sim[current_reference] = lil_matrix((1, len(all_sessions)), dtype=np.int8)
            vec = dict_items_sessions_sim[current_reference]
            vec[0,index_session] = 1
            dict_items_sessions_sim[current_reference] = vec
        
    return dict_items_sessions_sim
            
def seq_rule(df_train_data):
    matrix_train_data = df_train_data.values
    current_session_id = 0; dict_seq_r = dict();current_reference = 0
    for i in range(len(matrix_train_data)):
        new_session_id = matrix_train_data[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_train_data[i,5]
            for k in range(len(past_references)):
                association_reference = str(past_references[k]) + '|' + str(new_reference)
                if association_reference in dict_seq_r:
                    dict_seq_r[association_reference] += 1/(len(past_references)-k)
                else:
                    dict_seq_r[association_reference] = 1/(len(past_references)-k)
            current_reference = new_reference
            past_references.append(current_reference)
        else:
            past_references = list()
            distance_i_j = 1
            current_session_id = new_session_id
            current_reference = matrix_train_data[i,5]
            past_references = [current_reference]
    return dict_seq_r
            
def distance_impressions_item(df_train_data, df_test):
    
    df_total = pd.concat([df_train_data, df_test]).sort_values('timestamp')
    matrix_test_data_clickout = df_total[df_total['action_type'] == 'clickout item'].values
    dict_sim = dict()
    w = 2
    for i in range(len(matrix_test_data_clickout)):
        impression_list = matrix_test_data_clickout[i,10].split("|")
        for k, item in enumerate(impression_list):
            if k+w<len(impression_list):
                for j in range(w):
                    item_j = str(item) + '|' + str(impression_list[k+1+j])
                    inv_item_j = str(impression_list[k+1+j]) + '|' + str(item)
                    if item_j in dict_sim:
                        dict_sim[item_j] += 1/(j+1) 
                    elif inv_item_j in dict_sim:
                        dict_sim[inv_item_j] += 1/(j+1)
                    else:
                        dict_sim[item_j] = 1/(j+1)
            else:
                w_new = len(impression_list) - (k+1)
                for j in range(w_new):
                    item_j = str(item) + '|' + str(impression_list[k+1+j])
                    inv_item_j = str(impression_list[k+1+j]) + '|' + str(item)
                    if item_j in dict_sim:
                        dict_sim[item_j] += 1/(j+1) 
                    elif inv_item_j in dict_sim:
                        dict_sim[inv_item_j] += 1/(j+1)
                    else:
                        dict_sim[item_j] = 1/(j+1)
    return dict_sim
    
def click_user_past_sessions(df_train_data, df_test, phase='eval'):
    
    dict_click_user = dict()
    if phase == 'eval':
        bool_ = df_train_data['action_type'].isin(['clickout item'])
        df_tot_user_ref = df_train_data[bool_].sort_values('timestamp')
    else:
        df_total = pd.concat([df_train_data, df_test]).sort_values('timestamp')
        bool_ = df_total['action_type'].isin(['clickout item'])
        df_tot_user_ref = df_total[bool_].sort_values('timestamp')
        
    matrix_tot_user_ref = df_tot_user_ref[['user_id', 'reference']].values
    for i in range(len(matrix_tot_user_ref)):
        user_id = matrix_tot_user_ref[i,0]
        ref = matrix_tot_user_ref[i,1]
        if ref != 'NAN':
            if user_id in dict_click_user:
                appended_list = dict_click_user[user_id]
                #if not(ref in appended_list):
                appended_list.append(ref)
                dict_click_user[user_id] = appended_list
            else:
                dict_click_user[user_id] = [ref]
    
    return dict_click_user
            
def inter_user_past_sessions(df_train_data, df_test, phase = 'eval'):
    
    dict_inter_user = dict()
    if phase == 'eval':
        bool_ = df_train_data['action_type'].isin(['interaction item image', 'interaction item rating', 'interaction item info', 'interaction item deals', 'search for item'])
        df_tot_user_ref = df_train_data[bool_].sort_values('timestamp')
    else:
        df_total = pd.concat([df_train_data, df_test]).sort_values('timestamp')
        bool_ = df_train_data['action_type'].isin(['interaction item image', 'interaction item rating', 'interaction item info', 'interaction item deals', 'search for item'])
        df_tot_user_ref = df_total[bool_].sort_values('timestamp')

    matrix_tot_user_ref = df_tot_user_ref[['user_id', 'reference']].values
    for i in range(len(matrix_tot_user_ref)):
        user_id = matrix_tot_user_ref[i,0]
        ref = matrix_tot_user_ref[i,1]
        if user_id in dict_inter_user:
            appended_list = dict_inter_user[user_id]
            appended_list.append(ref)
            dict_inter_user[user_id] = appended_list
        else:
            dict_inter_user[user_id] = [ref]
    return dict_inter_user

def get_seq_corpus(df_train_data):
    
    matrix_train_data = df_train_data.values
    ### Create the corpus: the corpus corresponds to a list of successive actions
    current_session_id = 0; list_all_sessions = list();current_reference = 0; list_references = list(); current_action = 0
    for i in range(len(matrix_train_data)):
        new_session_id = matrix_train_data[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_train_data[i,5]
            new_action = matrix_train_data[i,4]
            if new_reference == current_reference and new_action == current_action:
                k = 0
            else:
                list_references.append(new_reference)
                current_reference = new_reference
                current_action = new_action
        else:
            list_all_sessions.append(list_references)
            current_session_id = new_session_id
            list_references = list()
            current_reference = matrix_train_data[i,5]
            current_action = matrix_train_data[i,4]
            list_references.append(current_reference)
            
    corpus = list_all_sessions[1:len(list_all_sessions)]
    
    return corpus
    
def get_seq_action_corpus(df_data):   
    
    matrix_data = df_data.values
    ### Create the corpus: the corpus corresponds to a list of successive actions
    current_session_id = 0; list_all_sessions_pair = list();current_reference = 0; list_references = list(); current_action = 0
    for i in range(len(matrix_data)):
        new_session_id = matrix_data[i,1]
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_data[i,5]
            new_action = matrix_data[i,4]
            if new_reference == current_reference and new_action == current_action:
                k = 0
            else:
                if new_action == 'clickout item' and new_reference == 'NAN':                    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item) + "|" +str(new_action))
                            list_all_sessions_pair.append(list_ref_copy)
                    current_reference = new_reference
                    current_action = new_action
                elif new_action == 'clickout item':    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item) + "|" +str(new_action))
                            list_all_sessions_pair.append(list_ref_copy)
                    list_references.append(str(new_reference) + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
                else:
                    list_references.append(str(new_reference) + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action                    
        else:
            list_all_sessions_pair.append(list_references)
            current_session_id = new_session_id
            list_references = list()
            current_reference = matrix_data[i,5]
            current_action = matrix_data[i,4]
            if current_action == 'clickout item' and current_reference == 'NAN':                    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item) + "|" +str(new_action))
                        list_all_sessions_pair.append(list_ref_copy)
            elif current_action == 'clickout item':    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item) + "|" +str(current_action))
                        list_all_sessions_pair.append(list_ref_copy)
                list_references.append(str(new_reference) + "|" +str(current_action))
            else:
                list_references.append(str(current_reference) + "|" +str(current_action))  

    corpus = list_all_sessions_pair[1:len(list_all_sessions_pair)]
        
    return corpus

def get_seq_labeled_only_action_BPR(df_data, Ns = 3, phase='train'):
    matrix_data = df_data.values
    ### Create the corpus: the corpus corresponds to a list of successive actions
    current_session_id = 0; list_all_sessions_pair = list();current_reference = 0; list_references = list(); current_action = 0
    for i in range(len(matrix_data)):
        new_session_id = matrix_data[i,1]
        new_user_id = matrix_data[i,0]
        new_step_id = matrix_data[i,3]
        key_id = str(new_session_id) + '|' + str(new_user_id) + '|' + str(new_step_id) 
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_data[i,5]
            new_action = matrix_data[i,4]
            if new_reference == current_reference  and new_action != 'clickout item' and new_action == current_action:
                k = 0
            else:
                if new_action == 'clickout item' and new_reference == 'NAN':                    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item))# + "|" +str(new_action))
                            list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
                    current_reference = new_reference
                    current_action = new_action
                elif new_action == 'clickout item':    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            if phase=='train':
                                lambda_ = abs(int(len(impression_list)/Ns))+1
                                if random.randint(0,lambda_) == 0:
                                    list_ref_copy = list_references.copy()
                                    #list_ref_copy.append(str(item))# + "|" +str(new_action))
                                    clickouts_els = [new_reference, item]
                                    list_all_sessions_pair.append((key_id, list_ref_copy, clickouts_els))
                            else:
                                    list_ref_copy = list_references.copy()
                                    #list_ref_copy.append(str(item))# + "|" +str(new_action))
                                    clickouts_els = [new_reference, item]
                                    list_all_sessions_pair.append((key_id, list_ref_copy, clickouts_els))                                
                        #else:
                        #    list_ref_copy = list_references.copy()
                        #    list_ref_copy.append(str(item))# + "|" +str(new_action))
                        #    list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                    list_references.append(str(new_reference))# + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
                else:
                    list_references.append(str(new_reference))# + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
        else:
            list_all_sessions_pair.append(list_references)
            current_session_id = new_session_id
            list_references = list()
            current_reference = matrix_data[i,5]
            current_action = matrix_data[i,4]
            if current_action == 'clickout item' and current_reference == 'NAN':                    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item))# + "|" +str(current_action))
                        list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
            elif current_action == 'clickout item':    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        if phase == 'train':
                            lambda_ = abs(int(len(impression_list)/Ns))+1
                            if random.randint(0, lambda_) == 0:
                                list_ref_copy = list_references.copy()
                                #list_ref_copy.append(str(item))# + "|" +str(current_action))
                                clickouts_els = [current_reference, item]
                                list_all_sessions_pair.append((key_id, list_ref_copy, clickouts_els))
                        else:
                                list_ref_copy = list_references.copy()
                                #list_ref_copy.append(str(item))# + "|" +str(current_action))
                                clickouts_els = [current_reference, item]
                                list_all_sessions_pair.append((key_id, list_ref_copy, clickouts_els))                            
                    #else:
                    #    list_ref_copy = list_references.copy()
                    #    list_ref_copy.append(str(item))# + "|" +str(current_action))
                    #    list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                list_references.append(str(current_reference))# + "|" +str(current_action))
            else:
                list_references.append(str(current_reference))# + "|" +str(current_action))

    corpus = list_all_sessions_pair[1:len(list_all_sessions_pair)]
    
    return corpus

def get_seq_labeled_(df_data, Ns = 3, phase='train'):
    
    matrix_data = df_data.values
    ### Create the corpus: the corpus corresponds to a list of successive actions
    current_session_id = 0; list_all_sessions_pair = list();current_reference = 0; list_references = list(); current_action = 0
    for i in range(len(matrix_data)):
        new_session_id = matrix_data[i,1]
        new_user_id = matrix_data[i,0]
        new_step_id = matrix_data[i,3]
        key_id = str(new_session_id) + '|' + str(new_user_id) + '|' + str(new_step_id) 
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_data[i,5]
            new_action = matrix_data[i,4]
            if new_reference == current_reference and new_action == current_action and new_action != 'clickout item':
                k = 0
            else:
                if new_action == 'clickout item' and new_reference == 'NAN':                    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item) + "|" +str(new_action))
                            list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
                    current_reference = new_reference
                    current_action = new_action
                elif new_action == 'clickout item':    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            if phase=='train':
                                lambda_ = abs(int(len(impression_list)/Ns))+1
                                if random.randint(0,lambda_) == 0:
                                    list_ref_copy = list_references.copy()
                                    list_ref_copy.append(str(item) + "|" +str(new_action))
                                    list_all_sessions_pair.append((key_id, list_ref_copy, 0))
                            else:
                                    list_ref_copy = list_references.copy()
                                    list_ref_copy.append(str(item) + "|" +str(new_action))
                                    list_all_sessions_pair.append((key_id, list_ref_copy, 0))                                
                        else:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item) + "|" +str(new_action))
                            list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                    list_references.append(str(new_reference) + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
                else:
                    list_references.append(str(new_reference) + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
        else:
            list_all_sessions_pair.append(list_references)
            current_session_id = new_session_id
            list_references = list()
            current_reference = matrix_data[i,5]
            current_action = matrix_data[i,4]
            if current_action == 'clickout item' and current_reference == 'NAN':                    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item) + "|" +str(current_action))
                        list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
            elif current_action == 'clickout item':    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        if phase == 'train':
                            lambda_ = abs(int(len(impression_list)/Ns))+1
                            if random.randint(0, lambda_) == 0:
                                list_ref_copy = list_references.copy()
                                list_ref_copy.append(str(item) + "|" +str(current_action))
                                list_all_sessions_pair.append((key_id, list_ref_copy, 0))
                        else:
                                list_ref_copy = list_references.copy()
                                list_ref_copy.append(str(item) + "|" +str(current_action))
                                list_all_sessions_pair.append((key_id, list_ref_copy, 0))                            
                    else:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item) + "|" +str(current_action))
                        list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                list_references.append(str(current_reference) + "|" +str(current_action))
            else:
                list_references.append(str(current_reference) + "|" +str(current_action))

    corpus = list_all_sessions_pair[1:len(list_all_sessions_pair)]
    
    return corpus

def get_seq_labeled_only_action(df_data, Ns = 3, phase='train'):
    matrix_data = df_data.values
    ### Create the corpus: the corpus corresponds to a list of successive actions
    current_session_id = 0; list_all_sessions_pair = list();current_reference = 0; list_references = list(); current_action = 0
    for i in range(len(matrix_data)):
        new_session_id = matrix_data[i,1]
        new_user_id = matrix_data[i,0]
        new_step_id = matrix_data[i,3]
        key_id = str(new_session_id) + '|' + str(new_user_id) + '|' + str(new_step_id) 
        ## if the same session
        if new_session_id == current_session_id:
            new_reference = matrix_data[i,5]
            new_action = matrix_data[i,4]
            if new_reference == current_reference  and new_action != 'clickout item' and new_action == current_action:
                k = 0
            else:
                if new_action == 'clickout item' and new_reference == 'NAN':                    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item))# + "|" +str(new_action))
                            list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
                    current_reference = new_reference
                    current_action = new_action
                elif new_action == 'clickout item':    
                    impression_list = matrix_data[i,10].split('|')
                    for l, item in enumerate(impression_list):
                        if item != new_reference:
                            if phase=='train':
                                lambda_ = abs(int(len(impression_list)/Ns))+1
                                if random.randint(0,lambda_) == 0:
                                    list_ref_copy = list_references.copy()
                                    list_ref_copy.append(str(item))# + "|" +str(new_action))
                                    list_all_sessions_pair.append((key_id, list_ref_copy, 0))
                            else:
                                    list_ref_copy = list_references.copy()
                                    list_ref_copy.append(str(item))# + "|" +str(new_action))
                                    list_all_sessions_pair.append((key_id, list_ref_copy, 0))                                
                        else:
                            list_ref_copy = list_references.copy()
                            list_ref_copy.append(str(item))# + "|" +str(new_action))
                            list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                    list_references.append(str(new_reference))# + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
                else:
                    list_references.append(str(new_reference))# + "|" +str(new_action))
                    current_reference = new_reference
                    current_action = new_action
        else:
            list_all_sessions_pair.append(list_references)
            current_session_id = new_session_id
            list_references = list()
            current_reference = matrix_data[i,5]
            current_action = matrix_data[i,4]
            if current_action == 'clickout item' and current_reference == 'NAN':                    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item))# + "|" +str(current_action))
                        list_all_sessions_pair.append((key_id, list_ref_copy, 'test'))
            elif current_action == 'clickout item':    
                impression_list = matrix_data[i,10].split('|')
                for l, item in enumerate(impression_list):
                    if item != current_reference:
                        if phase == 'train':
                            lambda_ = abs(int(len(impression_list)/Ns))+1
                            if random.randint(0, lambda_) == 0:
                                list_ref_copy = list_references.copy()
                                list_ref_copy.append(str(item))# + "|" +str(current_action))
                                list_all_sessions_pair.append((key_id, list_ref_copy, 0))
                        else:
                                list_ref_copy = list_references.copy()
                                list_ref_copy.append(str(item))# + "|" +str(current_action))
                                list_all_sessions_pair.append((key_id, list_ref_copy, 0))                            
                    else:
                        list_ref_copy = list_references.copy()
                        list_ref_copy.append(str(item))# + "|" +str(current_action))
                        list_all_sessions_pair.append((key_id, list_ref_copy, 1))
                list_references.append(str(current_reference))# + "|" +str(current_action))
            else:
                list_references.append(str(current_reference))# + "|" +str(current_action))

    corpus = list_all_sessions_pair[1:len(list_all_sessions_pair)]
    
    return corpus

def train_w2vec_model(corpus, size=100, window=4, min_count=5, workers=4):
    
    # define training data
    sentences = corpus
    # train model
    model = Word2Vec(sentences, size=size, window=window, min_count=min_count, workers=workers, iter=15)
    # summarize vocabulary
    words = model.wv.vocab
    
    return model ,words


def pos_neg_sample(df_train_data, df_test, dict_hotel_property):
    ## Training part
    bool_solely_session_train = (df_train_data['action_type'] == 'clickout item') & (df_train_data['step'] == 1)
    df_solely_session = df_train_data[bool_solely_session_train]
    matrix_cxt_data = df_solely_session[['reference', 'platform', 'device', 'impressions', 'prices']].values
    negative_sample = list()
    positive_sample = list()
    for i in range(len(matrix_cxt_data)):
        impressions = matrix_cxt_data[i,3].split('|')
        prices = matrix_cxt_data[i,4].split('|')
        if matrix_cxt_data[i,0] in impressions:
            price_item = prices[impressions.index(matrix_cxt_data[i,0])]
        else:
            price_item = np.mean(np.array(prices).astype(int))
        cnt = 0
        positive_sample.append(np.array([matrix_cxt_data[i,0], int(price_item), matrix_cxt_data[i,1], matrix_cxt_data[i,2], 1]))
        for k, item in enumerate(impressions):
            if item != matrix_cxt_data[i,0]:
                cnt += 1
                negative_sample.append(np.array([item, int(prices[k]), matrix_cxt_data[i,1], matrix_cxt_data[i,2], 0]))
                if cnt > 4:
                    break
    neg_sample = np.array(negative_sample)
    pos_sample = np.array(positive_sample)
    
    ## Test part
    df_test.reference.fillna('NAN', inplace=True)
    df_sub = df_test[(df_test['reference'] == 'NAN') &  (df_test['action_type'] == 'clickout item')]
    df_eval = df_test[(df_test['reference'] != 'NAN') &  (df_test['action_type'] == 'clickout item')]
    matrix_sub = df_sub[['platform', 'device', 'impressions', 'prices']].values
    ### pred
    pred_sample = list()
    for i in range(len(matrix_sub)):
        impressions = matrix_sub[i,2].split('|')
        prices = matrix_sub[i,3].split('|')
        for k, item in enumerate(impressions):
            pred_sample.append(np.array([item, int(prices[k]), matrix_sub[i,0], matrix_sub[i,1]]))
    pred_sample = np.array(pred_sample)
    ### eval
    matrix_eval = df_eval[['platform', 'device', 'impressions', 'prices']].values
    eval_sample = list()
    for i in range(len(matrix_eval)):
        impressions = matrix_eval[i,2].split('|')
        prices = matrix_eval[i,3].split('|')
        for k, item in enumerate(impressions):
            eval_sample.append(np.array([item, int(prices[k]), matrix_eval[i,0], matrix_eval[i,1]]))
    eval_sample = np.array(eval_sample)
    
    x_data = np.concatenate([pos_sample[:,0:pos_sample.shape[1]-1], neg_sample[:,0:pos_sample.shape[1]-1], pred_sample, eval_sample], axis=0)
    y = np.concatenate([pos_sample, neg_sample], axis=0)[:,pos_sample.shape[1]-1].astype(int)
    
    return x_data, y, pos_sample.shape, neg_sample.shape, pred_sample.shape, eval_sample.shape

def one_hot_enc(x_data, dict_hotel_property):   
    most_freq_prt = np.zeros((1,len(dict_prt_occ)))
    cnt = 0
    for i in range(len(dict_prt_occ)):
        most_freq_prt[0,i] = 1.0
        cnt+=1
        if cnt > 40:
            break
    ## One hot encoding by myself
    list_platform = dict(zip(np.unique(x_data[:,2]), np.arange(0, len(np.unique(x_data[:,2])), 1)))
    list_devices = dict(zip(np.unique(x_data[:,3]), np.arange(0, len(np.unique(x_data[:,3])), 1)))
    list_new_data_x = list()
    for i in range(len(x_data)):
        if int(x_data[i,0]) in dict_hotel_property:
            properties_item = dict_hotel_property[int(x_data[i,0])]
        else:
            properties_item = most_freq_prt
        #
        platform_item = np.zeros((1, len(list_platform)))
        platform_item[0,list_platform[x_data[i,2]]] = 1.0
        #
        devices_item = np.zeros((1, len(list_devices)))
        devices_item[0,list_devices[x_data[i,3]]] = 1.0
        #
        new_x_data = np.concatenate([properties_item, platform_item, devices_item], axis=1)
        list_new_data_x.append(new_x_data)
        if not(i%10**6):
            print(i)
    return np.array(list_new_data_x)

def preprocess(x_pred):
    
    x_pred = x_pred[:,0,:]
    x_pred = np.concatenate([x_data[:,1].astype(float).reshape(-1,1), x_pred[:,1:x_pred.shape[1]] ], axis=1)
    for i in range(len(x_pred)):
        if x_pred[i,0] > 200.0:
            x_pred[i,0] = 200.0
    x_1 = x_pred[:,0].copy()
    scaler = MinMaxScaler()
    scaler.fit(x_1.reshape(-1,1))
    X = scaler.transform(x_1.reshape(-1,1))
    for i in range(len(x_pred)):
        x_pred[i,0] = X[i]
    
    return x_pred

## Now fill the dataframes df_eval and df_sub with the scores given by fm 
def fill_fm_values(x_pred, df_test):
    ## Test part
    df_test.reference.fillna('NAN', inplace=True)
    df_sub = df_test[(df_test['reference'] == 'NAN') &  (df_test['action_type'] == 'clickout item')]
    df_eval = df_test[(df_test['reference'] != 'NAN') &  (df_test['action_type'] == 'clickout item')]
    ### pred
    matrix_sub = df_sub[['platform', 'device', 'impressions']].values
    pred_sample_fm = list()
    sub_pred = fm.predict(sparse.csr_matrix(x_pred[len(y):len(y)+pred_sample_shape[0],:]))
    print(sub_pred.shape)
    cnt = 0
    for i in range(len(matrix_sub)):
        impressions = matrix_sub[i,2].split('|')
        list_score_fm = list()
        for item in impressions:
            list_score_fm.append(sub_pred[cnt])
            cnt += 1
        pred_sample_fm.append(list_score_fm)
    df_sub['fm_score'] = pred_sample_fm
    ### eval 
    matrix_eval = df_eval[['platform', 'device', 'impressions']].values
    eval_sample_fm = list()
    eval_pred = fm.predict(sparse.csr_matrix(x_pred[len(y)+pred_sample_shape[0]:len(y)+pred_sample_shape[0]+eval_sample_shape[0],:]))
    cnt = 0
    for i in range(len(matrix_eval)):
        impressions = matrix_eval[i,2].split('|')
        list_score_fm = list()
        for item in impressions:
            list_score_fm.append(eval_pred[cnt])
            cnt += 1
        eval_sample_fm.append(list_score_fm)
    df_eval['fm_score'] = eval_sample_fm
    return df_sub, df_eval

### Evaluate the fm technique
def fm_technique(df_eval):
    list_impressions = list()
    for i in range(len(matrix_eval)):
        impressions_test = matrix_eval[i,4].split('|')
        fm_score = np.array(matrix_eval[i,6].strip('[').strip(']').split(', ')).astype(float)
        df_2_sort = pd.DataFrame(data=fm_score)
        descending_arg_sort = list(df_2_sort.sort_values(by=0, ascending=False, kind= 'mergesort').index)
        impressions_test = [impressions_test[j] for j in descending_arg_sort]
        list_impressions.append(" ".join(impressions_test))
    return list_impressions

### fm technique + last interacted
def fm_last_interacted_technique(matrix_eval):
    list_impressions = list()
    for i in range(len(matrix_eval)):
        # Step Number
        current_step = matrix_eval[i,2]
        impressions_test = matrix_eval[i,4].split('|')
        prices_test = matrix_eval[i,5].split('|')
        fm_score = np.array(matrix_eval[i,6].strip('[').strip(']').split(', ')).astype(float)
        user_id = matrix_eval[i,0]
        if current_step < 2:
            df_2_sort = pd.DataFrame(data=fm_score)
            descending_arg_sort = list(df_2_sort.sort_values(by=0, ascending=False, kind= 'mergesort').index)
            impressions_test = [impressions_test[j] for j in descending_arg_sort]
            list_impressions.append(" ".join(impressions_test))
        # if not we see the previous actions
        else:
            step_moins_1 = current_step - 1
            user_session = User_Session_Step_eval[i][0:len(User_Session_Step_eval[i])-len(str(current_step))]
            EL_Moins_1 = dict_test[str(user_session) + str(step_moins_1)].split(';')
            if EL_Moins_1[3] in impressions_test:
                impressions_test.remove(EL_Moins_1[3])
                impressions_test.insert(0, EL_Moins_1[3])
                
                list_impressions.append(" ".join(impressions_test))
            else:
                list_impressions.append(" ".join(impressions_test))
    return list_impressions

def eval_fm_technique(list_impressions, matrix_eval):
    MRR = 0
    cnt = 0
    dict_rank_index = dict()
    for i in range(len(matrix_eval)):
        click_item = matrix_eval[i,3]
        list_cr = list_impressions[i].split(" ")
        if str(click_item) in list_cr:
            rank = list_cr.index(str(click_item)) + 1
            dict_rank_index[str(i)] = rank
            MRR += 1/rank
        else:
            dict_rank_index[str(i)] = 'NAN'
            cnt += 1 
    print(cnt)
    MRR = MRR/(len(list_impressions)-cnt)
    return MRR, dict_rank_index

def fastest_cosine_sim(item1, item2):
    nzz_vec_1 = dict_items_sessions_sim[item1].nonzero()[1]
    nzz_vec_2 = dict_items_sessions_sim[item2].nonzero()[1]
    len_1 = len(nzz_vec_1)
    len_2 = len(nzz_vec_2)
    cnt = 0
    if len_1 < len_2:
        for k in nzz_vec_1:
            if k in nzz_vec_2:
                cnt += 1
    else:
        for k in nzz_vec_2:
            if k in nzz_vec_1:
                cnt += 1
    norm_1 = math.sqrt(len_1)
    norm_2 = math.sqrt(len_2)
    return cnt/(norm_1*norm_2)

def process_sequence(cell, inputs, masks, max_l):
    '''
    Processes a whole sequence with an RNN cell and returns its last hidden state.

    Parameters
    ----------
    cell: RNN Cell
    inputs: float32 Tensor of dimensions [batch_size, max_l, 300]
    masks: int Tensor of dimensions [batch_size, max_l, 1]
    Returns
    -------
    state: Float32 tensor of dimensions [batch_size, hidden_states]
    '''
    state = cell.zero_state(inputs)
    for i in range(max_l):
        state = cell(inputs[:, i], state)*masks[:, i] + (1.-masks[:, i])*state
    return state

class GRU:

    def __init__(self, input_size, hidden_states, activation=None, name=None):
        self._hidden_states = hidden_states
        self._input_size = input_size
        self._activation = activation or tf.tanh
        self._name = (name or "gru") + "/"
        self._gate_kernel = tf.get_variable(self._name + "gates/weights",
                                              shape=[input_size + self._hidden_states, 2 * self._hidden_states])
        self._gate_bias = tf.get_variable(self._name + "gates/bias", shape=[2 * self._hidden_states])
        self._candidate_kernel = tf.get_variable(self._name + "candidate/weights",
                                                   shape=[input_size + self._hidden_states, self._hidden_states])
        self._candidate_bias = tf.get_variable(self._name + "candidate/bias", shape=[self._hidden_states])

    def state_size(self):
        return self._hidden_states

    def output_size(self):
        return self._hidden_states

    def zero_state(self, inputs):
        batch_size = tf.shape(inputs)[0]
        return tf.zeros([batch_size, self.state_size()], dtype=tf.float32)

    def __call__(self, inputs, state):
        gate_inputs = tf.matmul(tf.concat([inputs[:,0,:], state], 1), self._gate_kernel)
        gate_inputs = tf.nn.bias_add(gate_inputs, self._gate_bias)

        value = tf.sigmoid(gate_inputs)
        r, u = tf.split(value, 2, axis=1)

        r_state = r * state

        candidate = tf.matmul(tf.concat([inputs[:,0,:], r_state], 1), self._candidate_kernel)
        candidate = tf.nn.bias_add(candidate, self._candidate_bias)

        c = self._activation(candidate)
        new_h = u * state + (1 - u) * c
        return new_h
    
class BatchToken:
    def __init__(self):
        self.__count = 0
    def incr(self):
        self.__count += 1
    def reset(self):
        self.__count = 0
    def val(self):
        return self.__count

    
class Dataset_BPR:
    
    def __init__(self, train, val, test, word2vec, max_l):#, hotel_prt):
        self.train = train
        self.val = val
        self.test = test
        self.word2vec = word2vec
        #self.hotel_prt = hotel_prt
        self.token = BatchToken()
        self.max_l = max_l

    def next_batch(self, batch_size, start, end):
        '''
        Returns next batch of training data of size batch_size (and shuffles the dataset at the beginning of every epoch)

        Parameters
        ----------
        batch_size : int

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        ## Code
        max_l = self.max_l
        res = self.train[start:end]
        batch_actions = [x[0] for x in res]
        batch_pos = [x[1] for x in res]
        batch_neg = [x[2] for x in res]
        ##
        batch_pos = [self.get_w2v(x)[0] for x in batch_pos]
        batch_neg = [self.get_w2v(x)[0] for x in batch_neg]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_pos, batch_neg, batch_m
    '''
    def test_batch(self):

        Returns test data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]

        res = self.test
        batch_y = [[x[1]] for x in res]
        batch_x = [x[0] for x in res]
        batch_x = [[self.get_w2v(y) for y in x] for x in batch_x]
        batch_x, batch_m = np.array([[np.zeros(50) for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_x]), np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_x])
        return batch_x, batch_m, batch_y
    '''
    def val_batch(self):
        '''
        Returns validation data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        max_l = self.max_l
        res = self.val
        batch_actions = [x[0] for x in res]
        batch_pos = [x[1] for x in res]
        batch_neg = [x[2] for x in res]
        ##
        batch_pos = [self.get_w2v(x)[0] for x in batch_pos]
        batch_neg = [self.get_w2v(x)[0] for x in batch_neg]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_pos, batch_neg, batch_m
    
    def get_w2v(self, w):
        '''
        Returns a vector for a given word
        
        Parameters
        ----------
        w : str

        Returns
        -------
        w2v: float32 numpy array of dimensions [300]
        '''
        if w in self.word2vec:
            w2v = [self.word2vec[w]]
        else:
            w2v = [0]
        return w2v
    
    def get_hotel_prt(self, w):
        if int(w) in self.hotel_prt:
            w2v = self.hotel_prt[int(w)]
        else:
            w2v = np.zeros(len(list_prts))
        return w2v    

class Dataset:
    
    def __init__(self, train, val, test, word2vec, max_l):#, hotel_prt):
        self.train = train
        self.val = val
        self.test = test
        self.word2vec = word2vec
        #self.hotel_prt = hotel_prt
        self.token = BatchToken()
        self.max_l = max_l

    def next_batch(self, batch_size, start, end):
        '''
        Returns next batch of training data of size batch_size (and shuffles the dataset at the beginning of every epoch)

        Parameters
        ----------
        batch_size : int

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        ## Code
        max_l = self.max_l
        res = self.train[start:end]
        batch_actions = [x[0] for x in res]
        batch_y = [[x[1]] for x in res]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_y, batch_m
    '''
    def test_batch(self):

        Returns test data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]

        res = self.test
        batch_y = [[x[1]] for x in res]
        batch_x = [x[0] for x in res]
        batch_x = [[self.get_w2v(y) for y in x] for x in batch_x]
        batch_x, batch_m = np.array([[np.zeros(50) for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_x]), np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_x])
        return batch_x, batch_m, batch_y
    '''
    def val_batch(self):
        '''
        Returns validation data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        max_l = self.max_l
        res = self.val
        batch_actions = [x[0] for x in res]
        batch_y = [[x[1]] for x in res]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_y, batch_m
    def get_w2v(self, w):
        '''
        Returns a vector for a given word
        
        Parameters
        ----------
        w : str

        Returns
        -------
        w2v: float32 numpy array of dimensions [300]
        '''
        if w in self.word2vec:
            w2v = [self.word2vec[w]]
        else:
            w2v = [0]
        return w2v
    def get_hotel_prt(self, w):
        if int(w) in self.hotel_prt:
            w2v = self.hotel_prt[int(w)]
        else:
            w2v = np.zeros(len(list_prts))
        return w2v
    
class Dataset_Content:
    
    def __init__(self, train, val, test, word2vec, max_l, hotel_prt):
        self.train = train
        self.val = val
        self.test = test
        self.word2vec = word2vec
        self.hotel_prt = hotel_prt
        self.token = BatchToken()
        self.max_l = max_l

    def next_batch(self, batch_size, start, end):
        '''
        Returns next batch of training data of size batch_size (and shuffles the dataset at the beginning of every epoch)

        Parameters
        ----------
        batch_size : int

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        ## Code
        max_l = self.max_l
        res = self.train[start:end]
        batch_actions = [x[0] for x in res]
        batch_y = [[x[1]] for x in res]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        batch_hotel_content = [[self.get_hotel_prt(y[0]) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_hotel_content = np.array([[np.zeros(157) for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_hotel_content])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_y, batch_hotel_content, batch_m
    '''
    def test_batch(self):

        Returns test data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]

        res = self.test
        batch_y = [[x[1]] for x in res]
        batch_x = [x[0] for x in res]
        batch_x = [[self.get_w2v(y) for y in x] for x in batch_x]
        batch_x, batch_m = np.array([[np.zeros(50) for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_x]), np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_x])
        return batch_x, batch_m, batch_y
    '''
    def val_batch(self):
        '''
        Returns validation data

        Returns
        -------
        batch_x: float32 numpy array of dimensions [batch_size, max_l, 300]
        batch_m: int numpy array of dimensions [batch_size, max_l, 1]
        batch_y: int numpy array of dimensions [batch_size, 1]
        '''
        max_l = self.max_l
        res = self.val
        batch_actions = [x[0] for x in res]
        batch_y = [[x[1]] for x in res]
        ##
        batch_actions = [[self.get_w2v(y) for y in x] for x in batch_actions]
        batch_hotel_content = [[self.get_hotel_prt(y[0]) for y in x] for x in batch_actions]
        ##
        batch_actions = np.array([[[0] for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_actions])
        batch_hotel_content = np.array([[np.zeros(157) for i in range(max_l - len(x[:max_l]))] + x[:max_l] for x in batch_hotel_content])
        batch_m = np.array([[[0.] for i in range(max_l - len(x))] + [[1.] for i in range(len(x[:max_l]))] for x in batch_actions])
        return batch_actions, batch_y, batch_hotel_content, batch_m
    
    def get_w2v(self, w):
        '''
        Returns a vector for a given word
        
        Parameters
        ----------
        w : str

        Returns
        -------
        w2v: float32 numpy array of dimensions [300]
        '''
        if w in self.word2vec:
            w2v = [self.word2vec[w]]
        else:
            w2v = [0]
        return w2v
    
    def get_hotel_prt(self, w):
        if int(w) in self.hotel_prt:
            w2v = self.hotel_prt[int(w)]
        else:
            w2v = np.zeros(self.hotel_prt[5501].shape[0])
        return w2v

class VanillaRNN:

    def __init__(self, input_size, hidden_states, activation=None, name=None):
        self._hidden_states = hidden_states
        self._input_size = input_size
        self._activation = activation or tf.tanh
        self._name = (name or "vanilla_rnn") + "/"
        self._candidate_kernel = tf.get_variable(self._name + "candidate/weights",
                                                   shape=[input_size + self._hidden_states, self._hidden_states])
        self._candidate_bias = tf.get_variable(self._name + "candidate/bias", shape=[self._hidden_states])

    def state_size(self):
        return self._hidden_states

    def zero_state(self, inputs):
        batch_size = tf.shape(inputs)[0]
        return tf.zeros([batch_size, self.state_size()], dtype=tf.float32)

    def __call__(self, inputs, state):

        candidate = tf.matmul(tf.concat([inputs, state], 1), self._candidate_kernel)
        candidate = tf.nn.bias_add(candidate, self._candidate_bias)
        new_h = self._activation(candidate)
        return new_h
    
    
def save_obj(obj, name):
    with open('../train_data/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('../train_data/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
