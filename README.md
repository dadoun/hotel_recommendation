# Hotel_Recommendation

A python implementation of a multi-architecture neural network framework proposed to enhance hotel recommendations in Trivago platform (!https://recsys.trivago.cloud/). Submitted to the International RecSys'19 Workshop  (RecSys WS'19).
